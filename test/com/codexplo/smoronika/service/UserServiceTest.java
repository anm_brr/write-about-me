package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/20/12
 * Time: 11:50 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/resources/applicationContext*.xml")
public class UserServiceTest {
    @Inject
    UserService userService;

    @Test
    public void testRegisterUser() throws Exception {
        User user = new User();
        user.setUsername("anm_brr");
        user.setFirstName("Bazlur");
        user.setLastName("Rahman");
        user.setEmail("anm_brr@yahoo.com");
        user.setPassword("rokonoid12");
        userService.registerUser(user);
    }

    @Test
    public void testFindByLogin() throws Exception {
        User user = userService.findByLogin("anm_brr");
        Assert.assertNotNull(user);
    }
}
