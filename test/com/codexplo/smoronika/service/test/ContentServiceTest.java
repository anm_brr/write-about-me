package com.codexplo.smoronika.service.test;

import com.codexplo.smoronika.domain.Content;
import com.codexplo.smoronika.service.ContentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/16/12
 * Time: 10:34 AM
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/resources/applicationContext*.xml")
public class ContentServiceTest {

    @Inject

    private ContentService m_contentService;

    @Test
    public void testGetContent() throws Exception {
        Content content = this.m_contentService.getContent((long) 1);
        assertNotNull(content);
    }
}
