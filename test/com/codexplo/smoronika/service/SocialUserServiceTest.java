package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.SocialUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/17/12
 * Time: 4:46 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/resources/applicationContext*.xml")
public class SocialUserServiceTest {
    private static String userID = "1006341";
    private static String providerID = "4646168";
    private static String providerUserID = "13216346";


    @Inject
    private SocialUserService socialUserService;

    @Test
    public void testFindUsersByUserId() throws Exception {

    }

    @Test
    public void testFindUsersByUserIdAndProviderId() throws Exception {

    }

    @Test
    public void testFindUsersByUserIdAndProviderUserId() throws Exception {

    }

    @Test
    public void testGetSocialUser() throws Exception {
        socialUserService.getSocialUser(userID, providerID, providerUserID);
    }

    @Test
    public void testFindUserPrimaryByUserIdAndProviderId() throws Exception {

    }

    @Test
    public void testSelectMaxRankByUserIdAndProviderId() throws Exception {
        Assert.assertNotNull(socialUserService.selectMaxRankByUserIdAndProviderId(userID, providerID));
    }

    @Test
    public void testFindUserIdsByProviderIdAndProviderUserId() throws Exception {
        Assert.assertNotNull(socialUserService.findUserIdsByProviderIdAndProviderUserId(providerID, providerUserID));

    }

    @Test
    public void testFindUserIdsByProviderIdAndProviderUserIds() throws Exception {
        Assert.assertNotNull(socialUserService.findUserIdsByProviderIdAndProviderUserId(providerID, providerUserID));
    }

    @Test
    public void testGetRank() throws Exception {
        Assert.assertNotNull(socialUserService.getRank(userID, providerID));
    }

    @Test
    public void testRemoveSocialUser() throws Exception {


    }


    @Test
    @Rollback(value = false)
    public void testCreateSocialUser() throws Exception {
        SocialUser socialUser = new SocialUser();
        socialUser.setAccessToken("accessToken");
        socialUser.setDisplayName("Bazlur Rahman Rokon");
        socialUser.setExpireTime(4643432165L);
        socialUser.setProfileUrl("asdfasdfasdfasdfsa");
        socialUser.setProviderId(providerID);
        socialUser.setProviderUserId(providerUserID);
        socialUser.setRank((long) 1);
        socialUser.setImageUrl("asdfasdfasdfasdf");
        socialUser.setSecret("asdfasdfasdf");
        socialUser.setUserId(userID);
        //socialUserService.saveSocialUser(socialUser);
        //socialUserService.saveSocialUser(socialUser);

        Assert.assertNotNull(socialUserService.findUsersByUserId(userID));
    }

    @Test
    public void testSaveSocialUser() throws Exception {

    }
}
