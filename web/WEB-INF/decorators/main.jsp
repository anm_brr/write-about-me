<%--
  Created by IntelliJ IDEA.
  User: rokon
  Date: 12/13/12
  Time: 10:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
           prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><decorator:title/></title>
    <spring:url value="/resources/css/bootstrap.css" var="bootstrap"/>
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.css'/>" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap-wysihtml5.css'/>"/>

    <link rel="shortcut icon" href="<c:url value='/resources/favicon.ico'/>" type="image/x-icon">
    <link rel="icon" href="<c:url value='/resources/favicon.ico'/>" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/prettify.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap-wysihtml5.css'/>"/>

    <script type="text/javascript" src="<c:url value='/resources/js/wysihtml5-0.3.0.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-1.7.2.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/prettify.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/bootstrap-wysihtml5.js'/>"></script>


    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .container-narrow {
            margin: 0 auto;
            max-width: 700px;
        }

        .container-narrow>hr {
            margin: 30px 0;
        }

        .jumbotron {
            margin: 60px 0;
            text-align: center;
        }

        .jumbotron h1 {
            font-size: 72px;
            line-height: 1;
        }

        .jumbotron .btn {
            font-size: 21px;
            padding: 14px 24px;
        }

        .marketing {
            margin: 60px 0;
        }

        .marketing p+h4 {
            margin-top: 28px;
        }

        .form-signin {
            max-width: 400px;
            padding: 30px 30px 30px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e8e8e8;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        }

        .form-signin .form-signin-heading, .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin input[type="text"], .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

            /*.modal-body {*/
            /*max-height: 550px;*/
            /*width: 500px;*/
            /*}*/


    </style>
</head>
<body>
<div class="container-narrow">
    <div class="masthead">
        <ul class="nav nav-pills pull-right">
            <li class="active"><a
                    href="/"><fmt:message key="menu.home"/> </a></li>
            <li><a href="#"><fmt:message key="menu.aboutUs"/></a></li>
            <li><a href="#"><fmt:message key="menu.contact"/></a></li>

            <c:if test="${pageContext['request'].userPrincipal != null}">
                <spring:url value="/static/j_spring_security_logout" var="logout"/>
                <li><a href="profile/index"><fmt:message key="menu.profile"/></a></li>
                <li><a href="${logout}"> [<fmt:message key="menu.logout"/>] </a></li>
            </c:if>
            <c:if test="${pageContext['request'].userPrincipal == null}">
                <li><a href="#login_modal" data-toggle="modal"><fmt:message key="menu.login"/></a></li>
            </c:if>
        </ul>
        <h3 class="muted">
            <a href="${pageContext.request.servletPath}"> <fmt:message key="application.name"/> </a>
        </h3>
    </div>
    <hr>
    <hr>

    <decorator:body/>

    <hr>
    <hr>
    <div class="footer">
        <p align="center"><a href="/"><fmt:message key="application.name"/></a> .
            Brought to you by&nbsp;&nbsp;
            <a target="_blank" href="http://www.codexplo.net">Code Explosion</a> | <a
                    href="${pageContext.request.servletPath}/privacy"><fmt:message
                    key="menu.privacy"/> </a> | <a
                    href="?locale=en">
                <fmt:message key="language.english"/>
            </a> | <a href="?locale=bn"><fmt:message key="language.bangla"/></a>
        </p>
    </div>
</div>

<div id="login_modal" class="modal large hide fade" tabindex="-1" role="dialog">

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <br/>

        <h2 align="center" class="form-signin-heading"><a href="/"> <fmt:message key="application.name"/> </a></h2>

    </div>
    <div class="modal-body">
        <div class="form-signin">
            <form id="signin" action="/static/j_spring_security_check" method="post">

                <h3><fmt:message key="menu.login"/></h3>
                <c:if test="${not empty param['error']}">
                    <div class="error"><fmt:message key="menu.incorrect.login"/>
                    </div>
                </c:if>
                <fieldset>
                    <label for="login"><fmt:message key="login.username"/> </label> <input
                        class="input-block-level" id="login" name="j_username" type="text"
                        autocorrect="off" autocapitalize="off"
                        <c:if test="${not empty signinErrorMessage}">value="${SPRING_SECURITY_LAST_USERNAME}"</c:if> />
                    <label for="password"><fmt:message key="login.password"/> </label> <input id="password"
                                                                                              name="j_password"
                                                                                              type="password"
                                                                                              class="input-block-level"
                        />

                    <label class="forCheckbox" for='j_spring_security_remember_me'>
                        <fmt:message key="menu.remember.me"/>
                        <input type='checkbox' name='_spring_security_remember_me'/>
                    </label>
                </fieldset>
                <p>
                    <button class="btn btn-primary" align="center" type="submit"><fmt:message key="menu.login"/>
                    </button>
                </p>
                <p>
                    <a href="<c:url value="/reset" />"><fmt:message key="menu.forget.password"/> </a>
                </p>
            </form>

            <a class="btn btn-primary" href="<c:url value="/connect/facebook" />"><img
                    src="<c:url value='/resources/img/glyphicons_410_facebook.png'/>" alt=""/> <fmt:message
                    key="menu.login.with.facebook"/></a>
            <br/> <br/>
            <a class="btn btn-primary" href="<c:url value="/connect/twitter" />"> <img
                    src="<c:url value='/resources/img/glyphicons_411_twitter.png'/>" alt=""/> <fmt:message
                    key="menu.login.with.twitter"/>
            </a>
            <br/><br/>
            <%--<form id="facebook_signin"--%>
            <%--action="<c:url value="/signin/facebook" />" method="post">--%>
            <%--<button class="btn btn-primary icon icon" type="submit"><img alt=""--%>
            <%--src="<c:url value='/resources/img/glyphicons_410_facebook.png'/>">--%>
            <%--<fmt:message key="menu.login.with.facebook"/>--%>
            <%--</button>--%>
            <%--</form>--%>
            <%--<form id="twitter_signin" action="<c:url value="connect/twitter" />"--%>
            <%--method="post">--%>
            <%--<button class="btn btn-primary" type="submit"><img alt=""--%>
            <%--src="<c:url value='/resources/img/glyphicons_411_twitter.png'/> ">--%>
            <%--<fmt:message key="menu.login.with.twitter"/>--%>
            <%--</button>--%>
            <%--</form>--%>


            <p><fmt:message key="menu.new.account"/> -- <a href="<c:url value="/signup"/>"><fmt:message
                    key="menu.signup"/> </a>
            </p>
        </div>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/> </a>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#login_modal').modal('hide');
    });
</script>

</body>
</html>