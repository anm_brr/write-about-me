<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><fmt:message key="menu.signup"/></title>


</head>
<body>


<form:form name="regForm" commandName="user" method="post" cssClass="form-horizontal">

    <fieldset>
        <legend><fmt:message key="menu.signup"/></legend>

        <div class="control-group">
            <form:label path="username" cssClass="control-label"><fmt:message key="user.username"/> </form:label>
            <div class="controls">
                <form:input path="username"/>
                <form:errors path="username" cssClass="text-error"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="username" cssClass="control-label"><fmt:message key="user.password"/> </form:label>
            <div class="controls">
                <form:password path="password"/>
                <form:errors path="password" cssClass="text-error"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="passwordConfirmed" cssClass="control-label"><fmt:message
                    key="user.passwordConfirmed"/>
            </form:label>
            <div class="controls">
                <form:password path="passwordConfirmed"/>
                <form:errors path="passwordConfirmed" cssClass="text-error"/>
            </div>
        </div>
        <div class="control-group">
            <form:label path="firstName" cssClass="control-label"><fmt:message key="user.firstName"/> </form:label>
            <div class="controls">
                <form:input path="firstName"/>
                <form:errors path="firstName" cssClass="text-error"/>
            </div>
        </div>
        <div class="control-group">
            <form:label path="lastName" cssClass="control-label"><fmt:message key="user.lastName"/> </form:label>
            <div class="controls">
                <form:input path="lastName"/>
                <form:errors path="lastName" cssClass="text-error"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="lastName" cssClass="control-label"><fmt:message key="user.email"/> </form:label>
            <div class="controls">
                <form:input path="email"/>
                <form:errors path="email" cssClass="text-error"/>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary"><fmt:message key="form.submit"/></button>
            </div>
        </div>
    </fieldset>

</form:form>

</body>
</html>