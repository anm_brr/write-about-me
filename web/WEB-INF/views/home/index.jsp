<%--
  Created by IntelliJ IDEA.
  User: Rokonoid
  Date: 11/29/12
  Time: 6:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="application.name"/> | <fmt:message key="menu.home"/></title>
</head>
<body>


<div class="jumbotron">
    <h1><fmt:message key="application.name"/></h1>

    <p class="lead">
        <fmt:message key="application.headline"/>
    </p>
    <a class="btn btn-large btn-success" href="#"> <fmt:message key="connect.with"/> </a>
    <br/> <br/>

    <p style="font-size: 14px; margin-left:10px;position:relative;top:2px">
        <fmt:message key="no.account.yet"/> -- <a href="<c:url value='/signup'/> "> <fmt:message key="menu.signup"/></a>
        <fmt:message
                key="start.sending.request"/>
    </p>

</div>
</body>

</html>