<%--
  Created by IntelliJ IDEA.
  User: rokon
  Date: 12/14/12
  Time: 9:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta content="text/html;charset=utf-8"/>
    <meta content="${content.title}">
    <title>${content.title}</title>

</head>
<body>
<div class="container-narrow">
    <hr>
    <div>
        <div class="padding">
            <table class="table table-condensed">

                <tr><h4><a
                        href="<c:url value="/content/show?contentId=${content.contentId}&user=${content.owner}"/>"> ${content.title}</a>
                </h4></tr>
                <tr><fmt:formatDate value="${content.dateCreated}" type="both" dateStyle="FULL"/></tr>

                <br/><br/>
                <tr>${content.content}</tr>
                <br/>

            </table>
        </div>
    </div>
</div>

<a href="<c:url value="/content/delete/${content.contentId}"/>"
   onclick="confirm('Are you sure you want to delete this content')"><fmt:message
        key="menu.delete"/> </a> | <a
        href="<c:url value="/content/update?contentId=${content.contentId}&user=${content.owner}"/>"> <fmt:message
        key="menu.update"/> </a>


<%--<c:if test="${empty content.publicURL}">--%>
<%--<a href="#public_url_modal" data-toggle="modal"><fmt:message key="create.public.url"/></a>--%>
<%--</c:if>--%>
<%--<c:if test="${not empty content.publicURL}">--%>
<%--<a href="<%request.getServerName();%>" charset="utf-8" target="_blank"> Public Share Link</a>--%>

<%--<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en"--%>
<%--data-via="<fmt:message key="application.name"/> " data-url="<% request.getServerName();%>">Tweet</a>--%>


<%--<script>!function (d, s, id) {--%>
<%--var js, fjs = d.getElementsByTagName(s)[0];--%>
<%--if (!d.getElementById(id)) {--%>
<%--js = d.createElement(s);--%>
<%--js.id = id;--%>
<%--js.src = "https://platform.twitter.com/widgets.js";--%>
<%--fjs.parentNode.insertBefore(js, fjs);--%>
<%--}--%>
<%--}(document, "script", "twitter-wjs");</script>--%>
<%--</c:if>--%>


<%--<div class="modal fade" id="public_url_modal" tabindex="-1" role="dialog" aria-hidden="true">--%>
<%--<div class="modal-header">--%>
<%--<a class="close" data-dismiss="modal">&times;</a>--%>
<%--<br/>--%>

<%--<h3><fmt:message--%>
<%--key="create.public.url"/></h3>--%>
<%--</div>--%>
<%--<div class="modal-body">--%>

<%--<form class="form-inline">--%>
<%--<div class="controls">--%>
<%--<label class="label-important">Please write english characer here</label> <br/>--%>
<%--<input id="public_url" class="input-xlarge" type="text" value="${content.title}"/>--%>
<%--<br/>--%>
<%--</div>--%>

<%--<div class="modal-footer">--%>
<%--<a href="#" class="btn" data-dismiss="modal">Close</a>--%>
<%--<button class="btn btn-primary" type="submit" onclick="submitForm()">--%>
<%--<fmt:message key="form.submit"/></button>--%>
<%--</div>--%>
<%--</form>--%>
<%--</div>--%>
<%--</div>--%>


<%--<script type="text/javascript" charset="utf-8">--%>
<%--$(document).ready(function () {--%>
<%--$('#public_url_modal_modal').modal('hide');--%>

<%--});--%>

<%--function submitForm() {--%>
<%--var publicUrl = jQuery("#public_url").val();--%>
<%--var url = "/content/publicurl/" + publicUrl + "/" + ${content.id}+"/";--%>
<%--console.log(url);--%>

<%--jQuery.ajax(url,--%>
<%--{--%>
<%--type: "POST"--%>
<%--});--%>
<%--}--%>

<%--</script>--%>

</body>
</html>