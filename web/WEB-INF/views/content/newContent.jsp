<%--
  Created by IntelliJ IDEA.
  User: Rokonoid
  Date: 11/29/12
  Time: 10:25 AM
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fi">
<head>

    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <title><fmt:message key="write.your.comments"/></title>

    <%--<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/prettify.css'/>"/>--%>
    <%--<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap-wysihtml5.css'/>"/>--%>

    <%--<script type="text/javascript" src="<c:url value='/resources/js/wysihtml5-0.3.0.js'/>"></script>--%>
    <%--<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.7.2.min.js'/>"></script>--%>
    <%--<script type="text/javascript" src="<c:url value='/resources/js/prettify.js'/>"></script>--%>
    <%--<script type="text/javascript" src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>--%>
    <%--<script type="text/javascript" src="<c:url value='/resources/js/bootstrap-wysihtml5.js'/>"></script>--%>

    <style type="text/css">
        .error {
            color: #000;
            background-color: #ffEEEE;
            border: 3px solid #ff0000;
            padding: 8px;
            margin: 16px;
        }
    </style>

</head>
<body>
<div>
    <div class="padding">
        <fieldset>
            <legend><fmt:message key="new.write-up"/></legend>
            <form:form cssClass="form-inline" commandName="content">
                <div><form:errors path="*" cssClass="error"/>
                </div>

                <div class="control-group">
                    <form:label path="title" cssClass="control-label"><fmt:message key="form.title"/> </form:label>
                    <div class="controls">
                        <form:input path="title" cssClass="input-xxlarge"/>
                        <form:errors path="title" cssClass="text-error"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label path="content" cssClass="control-label"><fmt:message
                            key="form.maincontents"/></form:label>
                    <div class="controls">
                        <form:textarea path="content" cssClass="richtext" cssStyle="width: 810px; height: 300px"/>
                        <form:errors path="content" cssClass="text-error"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-large btn-primary" type="submit"><fmt:message
                                key="form.submit"/></button>
                    </div>
                </div>
            </form:form>
        </fieldset>
    </div>
</div>
<div>
</div>
</body>
</html>