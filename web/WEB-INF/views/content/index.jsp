<%--
  Created by IntelliJ IDEA.
  User: rokon
  Date: 12/2/12
  Time: 6:20 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message key="application.name"/></title>
</head>
<body>
<div class="container-narrow">
    <hr>
    <div>
        <div class="padding">
            <table class="table table-condensed">
                <c:forEach var="item" items="${contents}">

                    <tr><h4><a
                            href="<c:url value="/content/show?contentId=${item.contentId}&user=${item.owner}"/>"> ${item.title}</a>
                    </h4></tr>
                    <tr><fmt:formatDate value="${item.dateCreated}" type="both" dateStyle="FULL"/></tr>


                    <br/><br/>
                    <tr>${item.content}</tr>
                    <br>
                    <tr><a href="<c:url value="/content/delete/${item.contentId}"/>"
                           onclick="confirm('Are you sure you want to delete this content')"><fmt:message
                            key="menu.delete"/> </a> | <a
                            href="<c:url value="/content/update?contentId=${content.contentId}&user=${content.owner}"/>"><fmt:message
                            key="menu.update"/> </a></tr>
                    <hr/>
                </c:forEach>
            </table>

            <div class="pagination">

                <ul class="pager">
                    <c:if test="${empty page || page lt 1}">
                        <c:set var="page" value="1"/>
                    </c:if>

                    <c:if test="${empty size || size lt 1}">
                        <c:set var="size" value="5"/>
                    </c:if>

                    <spring:message code="list_size" var="list_size" htmlEscape="false"/>
                    <li><c:out value="${list_size} "/></li>


                    <c:forEach var="i" begin="5" end="25" step="5">
                    <c:choose>
                    <c:when test="${size == i}">
                    <li><c:out value="${i}"/></li>
                    </c:when>
                    <c:otherwise>

                    <spring:url value="" var="sizeUrl">
                        <spring:param name="page" value="1"/>
                        <spring:param name="size" value="${i}"/>
                    </spring:url>

                    <li><a href="${sizeUrl}">${i}</a></li>
                    </c:otherwise>
                    </c:choose>
                        <c:out value=" "/>
                    </c:forEach>
                    <c:out value="| "/>


                    <c:if test="${page ne 1}">
                    <spring:url value="" var="first">
                        <spring:param name="page" value="1"/>
                        <spring:param name="size" value="${size}"/>
                    </spring:url>
                        <spring:url value="/resources/img/resultset_first.png" var="first_image_url"/>
                        <spring:message code="list_first" var="first_label" htmlEscape="false"/>
                    <li><a class="image" href="${first}" title="${fn:escapeXml(first_label)}">
                        <img alt="${fn:escapeXml(first_label)}" src="${first_image_url}"/>
                    </a></li>
                    </c:if>

                    <c:if test="${page gt 1}">
                    <spring:url value="" var="previous">
                        <spring:param name="page" value="${page - 1}"/>
                        <spring:param name="size" value="${size}"/>
                    </spring:url>
                        <spring:url value="/resources/img/resultset_previous.png" var="previous_image_url"/>
                        <spring:message code="list_previous" var="previous_label" htmlEscape="false"/>
                    <li>< <a class="image" href="${previous}" title="${fn:escapeXml(previous_label)}">
                        <img alt="${fn:escapeXml(previous_label)}" src="${previous_image_url}"/>
                    </a>/li>

                        </c:if>
                        <c:out value=" "/>
                        <spring:message code="list_page" arguments="${page},${maxPages}" argumentSeparator=","/>
                        <c:out value=" "/>
                        <c:if test="${page lt maxPages}">

                        <spring:url value="" var="next">
                            <spring:param name="page" value="${page + 1}"/>
                            <spring:param name="size" value="${size}"/>
                        </spring:url>

                            <spring:url value="/resources/img/resultset_next.png" var="next_image_url"/>
                            <spring:message code="list_next" var="next_label" htmlEscape="false"/>

                    <li><a class="image" href="${next}" title="${fn:escapeXml(next_label)}">
                        <img alt="${fn:escapeXml(next_label)}" src="${next_image_url}"/>
                    </a></li>
                    </c:if>

                    <c:if test="${page ne maxPages}">
                    <spring:url value="" var="last">
                        <spring:param name="page" value="${maxPages}"/>
                        <spring:param name="size" value="${size}"/>
                    </spring:url>
                        <spring:url value="/resources/img/resultset_last.png" var="last_image_url"/>
                        <spring:message code="list_last" var="last_label" htmlEscape="false"/>
                    <li><a class="image" href="${last}" title="${fn:escapeXml(last_label)}">
                        <img alt="${fn:escapeXml(last_label)}" src="${last_image_url}"/>
                    </a></li>
                    </c:if>
            </div>
        </div>
    </div>
</div>

</body>
</html>