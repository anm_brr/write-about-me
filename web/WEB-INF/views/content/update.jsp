<%--
  Created by IntelliJ IDEA.
  User: rokon
  Date: 12/14/12
  Time: 9:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="menu.update"/> - ${content.title} </title>
</head>
<body>

<div>
    <div class="padding">
        <fieldset>
            <legend><fmt:message key="update.write-up"/></legend>
            <form:form cssClass="form-inline" modelAttribute="content">
                <form:errors path="*" cssClass="error" element="div"/>
                <div class="control-group">
                    <form:label path="title" cssClass="control-label"><fmt:message key="form.title"/> </form:label>
                    <div class="controls">
                        <form:input path="title" cssClass="input-xxlarge"/>
                        <form:errors path="title" cssClass="error"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label path="content" cssClass="control-label"><fmt:message
                            key="form.maincontents"/></form:label>
                    <div class="controls">
                        <form:textarea path="content" cssClass="richtext" cssStyle="width: 810px; height: 300px"/>
                        <form:errors path="content" cssClass="error"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-large btn-primary" type="submit"><fmt:message
                                key="form.submit"/></button>
                    </div>
                </div>

            </form:form>
        </fieldset>
    </div>
</div>
<div>
</div>
</body>
</html>