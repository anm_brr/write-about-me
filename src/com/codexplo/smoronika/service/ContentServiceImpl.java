package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.Content;
import com.codexplo.smoronika.domain.Content_;
import com.codexplo.smoronika.repository.ContentRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 8:43 AM
 */

@Service
@Transactional
public class ContentServiceImpl implements ContentService {


    @Inject
    private ContentRepository m_contentRepository;

    @Override
    public Content saveContent(Content content) {
        return m_contentRepository.save(content);
    }

    @Override
    public Content updateContent(Content content) {
        return m_contentRepository.save(content);
    }

    @Override
    public List<Content> getAllContents() {
        return m_contentRepository.findAll();
    }

    @Override
    public List<Content> getAllContents(int firstResult,
                                        int maxResults) {
        return m_contentRepository.findAll(
                new org.springframework.data.domain.PageRequest(firstResult
                        / maxResults, maxResults)).getContent();
    }

    @Override
    public Content getContent(Long id) {
        return m_contentRepository.findOne(id);
    }

    @Override
    public List<Content> getContentByOwner(String name) {
        return null;
    }

    @Override
    public List<Content> getContentByAuthor() {
        return null;
    }

    @Override
    public Long getContentCount() {
        return m_contentRepository.count();
    }

    @Override
    public void deleteContent(String contentId) {
        Content content = getContentByContentId(contentId);
        if (content != null) m_contentRepository.delete(content);
    }

//    @Override
//    public void delete(Long id) {
//        m_contentRepository.delete(id);
//    }

    @Override
    public Content getContentByPublicUrl(final String publicUrl) {
        return m_contentRepository.findOne(new Specification<Content>() {
            @Override
            public Predicate toPredicate(Root<Content> contentRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<String> publicURL = contentRoot.get(Content_.publicURL);
                return criteriaBuilder.like(publicURL, publicUrl);
            }
        });
    }

    @Override
    public Content getContentByTitle(final String title) {
        return m_contentRepository.findOne(new Specification<Content>() {
            @Override
            public Predicate toPredicate(Root<Content> contentRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<String> titlePath = contentRoot.get(Content_.title);
                return criteriaBuilder.equal(titlePath, title);
            }
        });
    }

    @Override
    public Content getContentByContentId(final String contentId) {
        return m_contentRepository.findOne(new Specification<Content>() {
            @Override
            public Predicate toPredicate(Root<Content> contentRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<String> contentIdPath = contentRoot.get(Content_.contentId);
                return criteriaBuilder.equal(contentIdPath, contentId);
            }
        });
    }
}
