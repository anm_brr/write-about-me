package com.codexplo.smoronika.service;


import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.social.support.ClientHttpRequestFactorySelector;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/15/12
 * Time: 3:10 AM
 */
@Service
public class UrlShortenServiceImpl implements UrlShortenService {

    private RestTemplate restTemplate;

    public UrlShortenServiceImpl() {
        restTemplate = new RestTemplate(
                ClientHttpRequestFactorySelector.getRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new MappingJacksonHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public String shortenUrl(String url) {
        Map<String, String> request = new HashMap<String, String>();
        request.put("longUrl", url);
        LinkedHashMap<String, String> shortUrl = restTemplate.postForObject(
                "https://www.googleapis.com/urlshortener/v1/url", request,
                LinkedHashMap.class);
        return shortUrl.get("id");
    }
}
