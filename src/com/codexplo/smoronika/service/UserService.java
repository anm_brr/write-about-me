package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 10:29 AM
 */
public interface UserService {
    User findByLogin(String login);

    User findByEmailAddress(String email);

    void registerUser(User user);

    boolean varify(String login, String password);
}
