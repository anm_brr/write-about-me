package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.SocialUser;
import com.codexplo.smoronika.domain.SocialUser_;
import com.codexplo.smoronika.repository.SocialUserRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/16/12
 * Time: 9:24 AM
 */
@Service
public class SocialUserServiceImpl implements SocialUserService {
    @Inject
    private SocialUserRepository repository;

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<SocialUser> findUsersByUserId(final String userId) {
        return repository.findAll(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(socialUserRoot.get(SocialUser_.userId), userId);
            }
        });
    }

    @Override
    public List<SocialUser> findUsersByUserIdAndProviderId(final String userId, final String providerId) {
        return repository.findAll(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.userId), userId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId));
            }
        });
    }

    @Override
    public List<SocialUser> findUsersByUserIdAndProviderUserIds(String userId, MultiValueMap<String, String> providerUserIds) {
        if (providerUserIds.isEmpty()) {
            return Collections.emptyList();
        }

        Session hibernateSession = (Session) em.getDelegate();
        Criteria criteria = hibernateSession.createCriteria(SocialUser.class).add(Restrictions.eq("userId", userId));

        Disjunction orExp = Restrictions.disjunction();

        for (Map.Entry<String, List<String>> entry : providerUserIds
                .entrySet()) {
            String providerId = entry.getKey();

            LogicalExpression andExp = Restrictions.and(Restrictions.eq("providerId", providerId),
                    Restrictions.in("providerUserId", entry.getValue()));
            orExp.add(andExp);
        }

        criteria.add(orExp);
        return criteria.list();
    }

    @Override
    public SocialUser getSocialUser(final String userId, final String providerId, final String providerUserId) {
        return repository.findOne(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.userId), userId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerUserId), providerUserId));
            }
        });
    }

    @Override
    public List<SocialUser> findUserPrimaryByUserIdAndProviderId(String userId, String providerId) {
        return null;
    }

    @Override
    public Long selectMaxRankByUserIdAndProviderId(final String userId, final String providerId) {
        return repository.count(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.userId), userId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId));
            }
        });
    }

    @Override
    public List<String> findUserIdsByProviderIdAndProviderUserId(final String providerId, final String providerUserId) {
        List<SocialUser> socialUsers = repository.findAll(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerUserId), providerUserId));
            }
        });
        List<String> userIds = new ArrayList<String>();
        for (SocialUser user : socialUsers) {
            userIds.add(user.getUserId());
        }

        return userIds;
    }

    @Override
    public List<String> findUserIdsByProviderIdAndProviderUserIds(final String providerId, final Set<String> providerUserIds) {

        String hql = "select distinct su.userId from SocialUser su where su.providerId = :providerId and su.providerUserId in (:providerUserIds)";

        return new ArrayList<String>(em.createQuery(hql).setHint("providerId", providerId).setParameter("providerUserIds", providerUserIds).getResultList());
    }

    @Override
    public int getRank(String userId, String providerId) {
        return 0;
    }

    @Override
    public void removeSocialUser(final String userId, final String providerId) {
        SocialUser user = repository.findOne(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.userId), userId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId));
            }
        });
        if (user != null) {
            repository.delete(user);
        }
    }

    @Override
    public void removeSocialUser(final String userId, final String providerId, final String providerUserId) {
        SocialUser user = repository.findOne(new Specification<SocialUser>() {
            @Override
            public Predicate toPredicate(Root<SocialUser> socialUserRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.and(criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerId), providerId), criteriaBuilder.equal(socialUserRoot.get(SocialUser_.providerUserId), providerUserId));
            }
        });
        if (user != null) {
            repository.delete(user);
        }
    }

    @Override
    public SocialUser createSocialUser(String userId, String providerId, String providerUserId, Long rank, String displayName, String profileUrl, String imageUrl, String accessToken, String secret, String refreshToken, Long expireTime) {
        SocialUser user = getSocialUser(userId, providerId, providerUserId);
        if (user != null) return user;

        SocialUser socialUser = new SocialUser();
        socialUser.setUserId(userId);
        socialUser.setProviderId(providerUserId);
        socialUser.setRank(rank);
        socialUser.setDisplayName(displayName);
        socialUser.setAccessToken(accessToken);
        socialUser.setExpireTime(expireTime);
        socialUser.setImageUrl(imageUrl);
        socialUser.setProfileUrl(profileUrl);
        socialUser.setSecret(secret);


        return repository.save(socialUser);
    }

    @Override
    public SocialUser saveSocialUser(SocialUser user) {
        return repository.save(user);
    }

    @Override
    public void deleteSocialUser(SocialUser socialUser) {
        repository.delete(socialUser);
    }
}
