package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.User;
import com.codexplo.smoronika.domain.User_;
import com.codexplo.smoronika.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 10:34 AM
 */
@Service
public class UserServiceImpl implements UserService {
    private static final String PASSWORD_SALT = "a2v39z39?x51?";
    @Inject
    private UserRepository userRepository;
    @Autowired
    private MessageDigestPasswordEncoder messageDigestPasswordEncoder;

    @Override
    public User findByLogin(final String login) {
        return userRepository.findOne(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> userRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(userRoot.get(User_.username), login);
            }
        });
    }

    @Override
    public User findByEmailAddress(final String email) {
        return userRepository.findOne(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> userRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(userRoot.get(User_.email), email);
            }
        });
    }

    @Override
    public void registerUser(User user) {
        user.setPassword(encodePassword(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public boolean varify(String login, String password) {
        User user;
        if ((user = findByLogin(login)) != null || (user = findByEmailAddress(login)) != null) {
            String encoded = encodePassword(password);
            if (encoded.equals(user.getPassword())) return true;
        }

        return false;
    }

    private String encodePassword(String password) {
        return messageDigestPasswordEncoder.encodePassword(password, PASSWORD_SALT);
    }
}
