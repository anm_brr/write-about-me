package com.codexplo.smoronika.service;

import org.springframework.social.connect.UsersConnectionRepository;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 12:00 PM
 */
public interface UserConnectionRepository extends UsersConnectionRepository {
}
