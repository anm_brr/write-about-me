package com.codexplo.smoronika.service;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/15/12
 * Time: 3:10 AM
 */
public interface UrlShortenService {
    String shortenUrl(String url);
}
