package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.Content;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 8:37 AM
 */

public interface ContentService {

    Content updateContent(Content content);

    Content saveContent(Content content);

    List<Content> getAllContents();

    List<Content> getAllContents(int page, int max);

    Content getContent(Long id);

    List<Content> getContentByOwner(String name);

    List<Content> getContentByAuthor();

    Long getContentCount();

    //void delete(Long id);
    void deleteContent(String contentId);

    Content getContentByPublicUrl(String publicUrl);

    Content getContentByTitle(String title);

    Content getContentByContentId(String contentId);

}
