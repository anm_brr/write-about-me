package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.SocialUser;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 9:21 AM
 */

public class SocialUserConnectionRepositoryImpl implements ConnectionRepository {

    private String userId;
    private SocialUserService socialUserService;
    private ConnectionFactoryLocator connectionFactoryLocator;
    private TextEncryptor textEncryptor;


    public SocialUserConnectionRepositoryImpl(String userId, SocialUserService socialUserService, ConnectionFactoryLocator connectionFactoryLocator, TextEncryptor textEncryptor) {
        this.userId = userId;
        this.socialUserService = socialUserService;
        this.connectionFactoryLocator = connectionFactoryLocator;
        this.textEncryptor = textEncryptor;
    }

    @Override
    public MultiValueMap<String, Connection<?>> findAllConnections() {
        MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<String, Connection<?>>();
        List<SocialUser> socialUsers = socialUserService.findUsersByUserId(userId);
        for (SocialUser socialUser : socialUsers) {
            ConnectionData connectionData = toConnectionData(socialUser);
            Connection<?> connection = createConnection(connectionData);
            connections.add(connectionData.getProviderId(), connection);
        }

        return connections;
    }

    @Override
    public List<Connection<?>> findConnections(String providerId) {
        List<Connection<?>> connections = new ArrayList<Connection<?>>();

        List<SocialUser> socialUsers = socialUserService.findUsersByUserIdAndProviderId(userId, providerId);
        for (SocialUser socialUser : socialUsers) {
            ConnectionData connectionData = toConnectionData(socialUser);
            Connection<?> connection = createConnection(connectionData);
            connections.add(connection);
        }

        return connections;
    }

    @Override
    public <A> List<Connection<A>> findConnections(Class<A> apiType) {
        String providerId = connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();

        // do some lame stuff to make the casting possible
        List<?> connections = findConnections(providerId);
        return (List<Connection<A>>) connections;

    }

    @Override
    public MultiValueMap<String, Connection<?>> findConnectionsToUsers(MultiValueMap<String, String> stringStringMultiValueMap) {
        MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<String, Connection<?>>();

        List<SocialUser> allSocialUsers = socialUserService.findUsersByUserIdAndProviderUserIds(userId, stringStringMultiValueMap);
        for (SocialUser socialUser : allSocialUsers) {
            ConnectionData connectionData = toConnectionData(socialUser);
            Connection<?> connection = createConnection(connectionData);
            connections.add(connectionData.getProviderId(), connection);
        }

        return connections;
    }

    @Override
    public Connection<?> getConnection(ConnectionKey connectionKey) {

        SocialUser socialUser = socialUserService.getSocialUser(userId, connectionKey.getProviderId(), connectionKey.getProviderUserId());
        if (socialUser == null) {
            throw new NoSuchConnectionException(connectionKey);
        }
        return createConnection(toConnectionData(socialUser));
    }

    @Override
    public <A> Connection<A> getConnection(Class<A> apiType, String providerUserId) {
        String providerId = connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();
        SocialUser socialUser = socialUserService.getSocialUser(userId, providerId, providerUserId);
        if (socialUser == null) {
            throw new NoSuchConnectionException(new ConnectionKey(providerId, providerUserId));
        }
        return (Connection<A>) createConnection(toConnectionData(socialUser));
    }

    @Override
    public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {

        Connection<A> connection = findPrimaryConnection(apiType);
        if (connection == null) {
            String providerId = connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();
            throw new NotConnectedException(providerId);
        }
        return connection;
    }

    @Override
    public <A> Connection<A> findPrimaryConnection(Class<A> apiType) {
        String providerId = connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();

        List<SocialUser> socialUsers = socialUserService.findUserPrimaryByUserIdAndProviderId(userId, providerId);
        Connection<A> connection = null;
        if (socialUsers != null && !socialUsers.isEmpty()) {
            connection = (Connection<A>) createConnection(toConnectionData(socialUsers.get(0)));
        }

        return connection;
    }

    @Override
    @Transactional(readOnly = false)
    public void addConnection(Connection<?> connection) {
        ConnectionData connectionData = connection.createData();

        // check if this social account is already connected to a local account
        List<String> userIds = socialUserService.findUserIdsByProviderIdAndProviderUserId(
                connectionData.getProviderId(), connectionData.getProviderUserId()
        );
        if (!userIds.isEmpty()) {
            throw new DuplicateConnectionException(new ConnectionKey(connectionData.getProviderId(), connectionData.getProviderUserId()));
        }
        //check if this user already has a connected account for this provider
        List<SocialUser> socialUsers = socialUserService.findUsersByUserIdAndProviderId(userId, connectionData.getProviderId());
        if (!socialUsers.isEmpty()) {
            throw new DuplicateConnectionException(new ConnectionKey(connectionData.getProviderId(), connectionData.getProviderUserId()));
        }

        Long maxRank = socialUserService.selectMaxRankByUserIdAndProviderId(userId, connectionData.getProviderId());
        long nextRank = (maxRank == null ? 1 : maxRank + 1);

        SocialUser socialUser = new SocialUser();
        socialUser.setUserId(userId);
        socialUser.setProviderId(connectionData.getProviderId());
        socialUser.setProviderUserId(connectionData.getProviderUserId());
        socialUser.setRank(nextRank);
        socialUser.setDisplayName(connectionData.getDisplayName());
        socialUser.setProfileUrl(connectionData.getProfileUrl());
        socialUser.setImageUrl(connectionData.getImageUrl());

        // encrypt these values
        socialUser.setAccessToken(encrypt(connectionData.getAccessToken()));
        socialUser.setSecret(encrypt(connectionData.getSecret()));
        socialUser.setRefreshToken(encrypt(connectionData.getRefreshToken()));

        socialUser.setExpireTime(connectionData.getExpireTime());

        try {
            socialUserService.saveSocialUser(socialUser);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateConnectionException(new ConnectionKey(connectionData.getProviderId(), connectionData.getProviderUserId()));
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void updateConnection(Connection<?> connection) {
        ConnectionData connectionData = connection.createData();
        SocialUser socialUser = socialUserService.getSocialUser(userId, connectionData.getProviderId(), connectionData.getProviderUserId());
        if (socialUser != null) {
            socialUser.setDisplayName(connectionData.getDisplayName());
            socialUser.setProfileUrl(connectionData.getProfileUrl());
            socialUser.setImageUrl(connectionData.getImageUrl());

            socialUser.setAccessToken(encrypt(connectionData.getAccessToken()));
            socialUser.setSecret(encrypt(connectionData.getSecret()));
            socialUser.setRefreshToken(encrypt(connectionData.getRefreshToken()));

            socialUser.setExpireTime(connectionData.getExpireTime());
            socialUserService.saveSocialUser(socialUser);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void removeConnections(String providerId) {
        // TODO replace with bulk delete HQL
        List<SocialUser> socialUsers = socialUserService.findUsersByUserIdAndProviderId(userId, providerId);
        for (SocialUser socialUser : socialUsers) {
            socialUserService.deleteSocialUser(socialUser);
        }
    }

    @Override
    public void removeConnection(ConnectionKey connectionKey) {
        SocialUser socialUser = socialUserService.getSocialUser(userId, connectionKey.getProviderId(), connectionKey.getProviderUserId());
        if (socialUser != null) {
            socialUserService.deleteSocialUser(socialUser);
        }

    }

    private ConnectionData toConnectionData(SocialUser socialUser) {
        return new ConnectionData(socialUser.getProviderId(),
                socialUser.getProviderUserId(),
                socialUser.getDisplayName(),
                socialUser.getProfileUrl(),
                socialUser.getImageUrl(),

                decrypt(socialUser.getAccessToken()),
                decrypt(socialUser.getSecret()),
                decrypt(socialUser.getRefreshToken()),

                convertZeroToNull(socialUser.getExpireTime()));
    }

    private Connection<?> createConnection(ConnectionData connectionData) {
        ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(connectionData.getProviderId());
        return connectionFactory.createConnection(connectionData);
    }

    private Long convertZeroToNull(Long expireTime) {
        return (expireTime != null && expireTime == 0 ? null : expireTime);
    }

    private String decrypt(String encryptedText) {
        return (textEncryptor != null && encryptedText != null) ? textEncryptor.decrypt(encryptedText) : encryptedText;
    }

    private String encrypt(String text) {
        return (textEncryptor != null && text != null) ? textEncryptor.encrypt(text) : text;
    }
}
