package com.codexplo.smoronika.service;

import com.codexplo.smoronika.domain.SocialUser;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/16/12
 * Time: 9:23 AM
 */
public interface SocialUserService {
    List<SocialUser> findUsersByUserId(String userId);

    List<SocialUser> findUsersByUserIdAndProviderId(String userId, String providerId);

    List<SocialUser> findUsersByUserIdAndProviderUserIds(String userId, MultiValueMap<String, String> providerUserIds);

    SocialUser getSocialUser(String userId, String providerId, String providerUserId);

    List<SocialUser> findUserPrimaryByUserIdAndProviderId(String userId, String providerId);

    Long selectMaxRankByUserIdAndProviderId(String userId, String providerId);

    List<String> findUserIdsByProviderIdAndProviderUserId(String providerId, String providerUserId);

    List<String> findUserIdsByProviderIdAndProviderUserIds(String providerId, Set<String> providerUserIds);

    int getRank(String userId, String providerId);

    void removeSocialUser(String userId, String providerId);

    void removeSocialUser(String userId, String providerId, String providerUserId);

    SocialUser createSocialUser(

            String userId,

            String providerId,

            String providerUserId,

            Long rank,

            String displayName,

            String profileUrl,

            String imageUrl,

            String accessToken,

            String secret,

            String refreshToken,

            Long expireTime);

    SocialUser saveSocialUser(SocialUser user);

    void deleteSocialUser(SocialUser socialUser);
}
