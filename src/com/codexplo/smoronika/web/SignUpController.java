package com.codexplo.smoronika.web;

import com.codexplo.smoronika.domain.User;
import com.codexplo.smoronika.security.SecurityUtil;
import com.codexplo.smoronika.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 11:03 AM
 */
@Controller
@RequestMapping("/signup")
public class SignUpController {

    private static final String FORM_VIEW = "signup/signup";
    private static final String SUCCESS_VIEW = "redirect:/";
    @Autowired
    private UserService userService;
    @Autowired
    private SignInAdapter signInAdapter;


    @RequestMapping(method = RequestMethod.GET)
    public String getForm(NativeWebRequest request, @ModelAttribute User user) {
        String view = FORM_VIEW;

        // check if this is a new user signing in via Spring Social
        Connection<?> connection = ProviderSignInUtils.getConnection(request);
        if (connection != null) {
            // populate new User from social connection user profile
            UserProfile userProfile = connection.fetchUserProfile();
            user.setUsername(userProfile.getUsername());

            // you may want to perform validation on the User object here
            // to check if you are able to get all the data your app requires
            // from the social connection user profile
            // (some providers may not provide email address, for example)
            // if the User is not valid for your app you will need to send the user
            // to your sign up form with some notice that more data is required

            // in this demo app, we assume we get all the data we need
            // the user is automatically registered and signed in
            userService.registerUser(user);

            // finish social signup/login
            ProviderSignInUtils.handlePostSignUp(user.getUsername(), request);

            // sign the user in and send them to the user home page
            signInAdapter.signIn(user.getUsername(), connection, request);
            view = SUCCESS_VIEW;
        }

        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@ModelAttribute @Valid User user, BindingResult bindingResult) {
        //validatePassword(user, bindingResult);
        if (bindingResult.hasErrors()) {
            return FORM_VIEW;
        }

        if (!user.getPassword().equals(user.getPasswordConfirmed())) {
            bindingResult.rejectValue("passwordConfirmed", "user.passwordConfirmed.mismatched", "Password mismatched, please try again");
            return FORM_VIEW;
        }

        User userByLogin = userService.findByLogin(user.getUsername());

        if (userByLogin != null) {
            bindingResult.rejectValue("username", "user.username.exist", "Someone with this username has already signed up, please try other name");
            return FORM_VIEW;
        }

        User userByEmailAddress = userService.findByEmailAddress(user.getEmail());

        if (userByEmailAddress != null) {
            bindingResult.rejectValue("email", "user.username.email", "Someone with this email has already signed up, please try other name");
            return FORM_VIEW;
        }


        // register user
        userService.registerUser(user);

        // sign user in
        SecurityUtil.signInUser(user);

        // send to user home page
        return SUCCESS_VIEW;
    }

    private void validatePassword(User user, BindingResult result) {

    }

}
