package com.codexplo.smoronika.web;

import com.codexplo.smoronika.domain.Content;
import com.codexplo.smoronika.domain.ContentStatus;
import com.codexplo.smoronika.service.ContentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 10:11 AM
 */

@Controller
@RequestMapping("content")
@SessionAttributes(types = {Content.class}, value = {"content"})
public class ContentController {
    Logger logger = LoggerFactory.getLogger(ContentController.class);
    @Autowired
    private ContentService contentService;

    @RequestMapping(value = "/newContent", method = RequestMethod.GET)
    public String createForm(@RequestParam("user") String user, @RequestParam("requestId") String requestId, Model uiModel) {
        Content content = new Content();

         /* TODO find owner and requestId, if matched, proceed, else redirect error message; */
        content.setOwner(user);
        uiModel.addAttribute("content", content);
        //uiModel.addAttribute("user", user);
        //uiModel.addAttribute("requestId", requestId);
        return "content/newContent";
    }

    @RequestMapping(value = "/newContent", method = RequestMethod.POST)
    public String submitForm(@ModelAttribute @Valid Content content, BindingResult formBinding, Model uiModel, HttpServletRequest httpServletRequest) {
        if (formBinding.hasErrors()) {
            uiModel.addAttribute("content", new Content());
            return "content/newContent";
        }
        uiModel.asMap().clear();
        try {
            String url = URLEncoder.encode(content.getTitle(), "utf-8").replace("-", "\\s");
            content.setPublicURL(url);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Content contentSaved = contentService.saveContent(content);
        return "redirect:/content/show?contentId=" + encodeUrlPathSegment(contentSaved.getContentId(), httpServletRequest) + "&user=" + encodeUrlPathSegment(contentSaved.getOwner(), httpServletRequest);
    }

    @RequestMapping(value = "show", method = RequestMethod.GET, produces = "text/html")
    public String showByContentId(@RequestParam("contentId") String contentId, @RequestParam("user") String user, Model uiModel) {
        /*TODO Cross Check by contendId, username, Author and Owner*/

        if (!contentId.isEmpty()) {
            uiModel.addAttribute("content", contentService.getContentByContentId(contentId));
            return "content/show";
        }
        return "content/error";
    }

    @RequestMapping(value = "update", produces = "text/html")
    public String updateForm(@RequestParam("contentId") String contentId, @RequestParam("user") String user, Model uiModel) {
        /*TODO Cross Check by contendId, username, Author and Owner*/
        if (!contentId.isEmpty()) {
            uiModel.addAttribute("content", contentService.getContentByContentId(contentId));
            return "content/update";
        } else {
            return "content/error";
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, produces = "text/html")
    public String update(@Valid @ModelAttribute(value = "content") Content content, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {

        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("content", contentService.getContent(content.getId()));
            return "content/update";
        }
        uiModel.asMap().clear();
        Content contentUpdated = contentService.updateContent(content);
        return "redirect:/content/show?contentId=" + encodeUrlPathSegment(contentUpdated.getContentId(), httpServletRequest) + "&user=" + encodeUrlPathSegment(contentUpdated.getOwner(), httpServletRequest);
    }

    @RequestMapping(value = "delete/{contentId}")
    public String delete(@PathVariable("contentId") String contentId) {
        contentService.deleteContent(contentId);
        return "redirect:/content/index";
    }

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {

        if (page != null || size != null) {
            int sizeNo = size == null ? 5 : size.intValue();
            uiModel.addAttribute("contents", contentService.getAllContents(page == null ? 0 : (page.intValue() - 1) * size, sizeNo));
            float nrOfPages = contentService.getContentCount() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));

        } else {
            uiModel.addAttribute("contents", contentService.getAllContents());
        }

        return "content/index";
    }

    private String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);

        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return pathSegment;
    }

    /**
     * Deprecated, will be deleted in next release
     */

    @Deprecated
    @RequestMapping(value = "/publicurl/{publicUrl}/{id}/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void submitFormForPublicURL(@PathVariable String publicUrl, @PathVariable Long id) {

        logger.info("publci URL" + publicUrl);


        Content content = contentService.getContent(id);
        content.setPublicURL(publicUrl);
        contentService.updateContent(content);
    }

    @Deprecated
    @RequestMapping(value = "public/{url}", method = RequestMethod.GET, produces = "text/html")
    public String show(@PathVariable String url, Model uiModel) {

        uiModel.addAttribute("content", contentService.getContentByPublicUrl(url));

        return "content/show";
    }

    @Deprecated
    @RequestMapping(value = "/{title}", method = RequestMethod.GET, produces = "text/html")
    public String showByTitle(@PathVariable("title") String title, Model uiModel) {
        if (!title.isEmpty()) {
            uiModel.addAttribute("content", contentService.getContentByTitle(title));
            return "content/show";
        }
        return "content/error";
    }

    @Deprecated
    @RequestMapping(value = "show/{id}")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        try {
            if (id != null) {
                uiModel.addAttribute("content", contentService.getContent(id));
                return "content/show";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "content/error";
        }
        return "content/error";
    }

    /**
     * The codes bellow will be used later, i have an intention
     * to make things using some
     * javascript framework like backbone to use restful architecture
     * *
     */


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Content> findAll() {
        return contentService.getAllContents();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public final Content get(@PathVariable("id") final Long id) {
        return this.contentService.getContent(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Content create(@RequestBody Content content) {
        Assert.notNull(content);
        return contentService.saveContent(content);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Content content, @PathVariable String id) {
        Assert.isTrue(content.getId().equals(id));
        contentService.updateContent(content);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void remove(@PathVariable Long id) {
        Content content = contentService.getContent(id);
        content.setStatus(ContentStatus.TRASH);
        contentService.updateContent(content);
    }
}
