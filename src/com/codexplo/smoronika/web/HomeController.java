package com.codexplo.smoronika.web;

import com.codexplo.smoronika.domain.User;
import com.codexplo.smoronika.security.SecurityUtil;
import com.codexplo.smoronika.service.UserConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 6:22 AM
 */

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private UserConnectionRepository userConnectionRepository;

    @RequestMapping(value = "index", method = RequestMethod.GET, produces = "text/html")
    public String index(Model uiModel) {

        User user = SecurityUtil.getLoggedInUser();
        if (user != null) {
            uiModel.addAttribute("user", user);

            // check if the account is connected to any social network accounts
            ConnectionRepository connectionRepository = userConnectionRepository.createConnectionRepository(user.getUsername());
            // only Twitter and Facebook accounts are supported in this app, so check for those specifically
            List connections = connectionRepository.findConnections("twitter");

            // Spring Social allows multiple social network accounts to be connected to one local account
            // however in this app we are only providing a UI for users to connect one Twitter and one Facebook account

            // so, anything in the list means that the user has connected a Twitter account
            if (!connections.isEmpty()) {
                uiModel.addAttribute("twitterConnected", "true");
            }
            // same thing, for Facebook
            connections = connectionRepository.findConnections("facebook");
            if (!connections.isEmpty()) {
                uiModel.addAttribute("facebookConnected", "true");
            }
        }

        return "home/index";
    }
}
