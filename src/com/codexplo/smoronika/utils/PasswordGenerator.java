package com.codexplo.smoronika.utils;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/21/12
 * Time: 4:17 AM
 */
public class PasswordGenerator {
    public static String gen10DigitPassword() {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        String chSym = "@#$%^&*/\\?{}[]";
        StringBuilder builder = new StringBuilder();
        builder.append("");
        try {
            Random rand = new Random();
            int i = rand.nextInt(10);
            int j = rand.nextInt(100);
            int k = rand.nextInt(100);
            int l = rand.nextInt(10);
            int m = rand.nextInt(10);
            int chr1 = rand.nextInt(chars.length());
            int chr2 = rand.nextInt(chars.length());
            int chr3 = rand.nextInt(chars.length());
            int chr4 = rand.nextInt(chars.length());
            int char5 = rand.nextInt(chSym.length());

            builder.append(chars.charAt(chr1));
            builder.append(i);
            builder.append(chars.charAt(chr2));
            builder.append(j);
            builder.append(chars.charAt(chr3));
            builder.append(k);
            builder.append(chSym.charAt(char5));
            builder.append(chars.charAt(chr4));
            builder.append(l);
            builder.append(m);
            builder.append(chSym.charAt(char5));
        } catch (Exception ignored) {
        }
        return builder.toString();
    }


    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println("[" + i + "] " + gen10DigitPassword());
        }
    }
}
