package com.codexplo.smoronika.repository;

import com.codexplo.smoronika.domain.SocialUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/16/12
 * Time: 12:09 AM
 */
@Repository
public interface SocialUserRepository extends JpaRepository<SocialUser, Long>, JpaSpecificationExecutor<SocialUser> {
}
