package com.codexplo.smoronika.social;

import com.codexplo.smoronika.domain.User;
import com.codexplo.smoronika.service.UserService;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 10:59 AM
 */
public class ConnectionSignUpImpl implements ConnectionSignUp {
    @Inject
    private UserService userService;

    @Override
    public String execute(Connection<?> connection) {
        UserProfile profile = connection.fetchUserProfile();
        String userId = null;
        if (profile.getEmail() != null) {
            // copy the username from the connection
            userId = profile.getUsername();

            // create the user
            User user = new User();
            user.setUsername(userId);
            userService.registerUser(user);
        }
        return userId;
    }
}
