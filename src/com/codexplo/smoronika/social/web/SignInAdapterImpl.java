package com.codexplo.smoronika.social.web;

import com.codexplo.smoronika.domain.User;
import com.codexplo.smoronika.security.SecurityUtil;
import com.codexplo.smoronika.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 11:01 AM
 */
public class SignInAdapterImpl implements SignInAdapter {
    @Inject
    private UserService userService;
    @Inject
    private TokenBasedRememberMeServices tokenBasedRememberMeServices;

    public String signIn(String userId, Connection<?> connection, NativeWebRequest request) {
        User user = userService.findByLogin(userId);
        Authentication authentication = SecurityUtil.signInUser(user);

        // set remember-me cookie
        tokenBasedRememberMeServices.onLoginSuccess(
                (HttpServletRequest) request.getNativeRequest(),
                (HttpServletResponse) request.getNativeResponse(),
                authentication);

        return null;
    }
}
