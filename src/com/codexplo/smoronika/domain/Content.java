package com.codexplo.smoronika.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 6:47 AM
 */

@Entity
public class Content extends BaseEntity {

    @Size(max = 65536)
    @Column(length = 65536, columnDefinition = "longtext")
    @NotEmpty
    private String content;
    private String contentId;
    private String author;
    private String owner;
    @NotEmpty
    @Size(max = 1024)
    @Column(length = 1024)
    private String title;
    private ContentStatus status;
    private ContentVisibility visibility;
    @Size(max = 2048)
    @Column(length = 2048)
    private String publicURL;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ContentStatus getStatus() {
        return status;
    }

    public void setStatus(ContentStatus status) {
        this.status = status;
    }

    public ContentVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(ContentVisibility visibility) {
        this.visibility = visibility;
    }

    public String getPublicURL() {
        return publicURL;
    }

    public void setPublicURL(String publicURL) {
        this.publicURL = publicURL;
    }

    @Override
    @PrePersist
    public void prePersist() {
        setContentId(UUID.randomUUID().toString());
        super.prePersist();
    }
}
