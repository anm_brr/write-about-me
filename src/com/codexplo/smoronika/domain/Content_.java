package com.codexplo.smoronika.domain;


import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/15/12
 * Time: 4:52 AM
 */
@StaticMetamodel(Content.class)
public class Content_ {
    public static volatile SingularAttribute<Content, String> publicURL;
    public static volatile SingularAttribute<Content, String> title;
    public static volatile SingularAttribute<Content, String> contentId;
}
