package com.codexplo.smoronika.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/16/12
 * Time: 2:33 AM
 */
@StaticMetamodel(SocialUser.class)
public class SocialUser_ {
    public static volatile SingularAttribute<SocialUser, String> userId;
    public static volatile SingularAttribute<SocialUser, String> providerId;
    public static volatile SingularAttribute<SocialUser, String> providerUserId;

}
