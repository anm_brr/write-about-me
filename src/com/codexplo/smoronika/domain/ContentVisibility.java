package com.codexplo.smoronika.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 8:28 AM
 */

public enum ContentVisibility {
    PUBLIC, PRIVATE, PASSWORD_PROTECTED;
}
