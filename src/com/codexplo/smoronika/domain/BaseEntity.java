package com.codexplo.smoronika.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 6:48 AM
 */

@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "id", sequenceName = "smoronika_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    private Long id;
    private Date dateCreated;
    private Date dateLastUpdated;
    @Version
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated(Date dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @PrePersist
    public void prePersist() {
        setDateCreated(new Date());
    }

    @PreUpdate
    public void preUpdate() {
        setDateLastUpdated(new Date());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseEntity)) {
            return false;
        }

        BaseEntity that = (BaseEntity) o;

        if (!dateCreated.equals(that.dateCreated)) {
            return false;
        }
        if (dateLastUpdated != null ? !dateLastUpdated.equals(that.dateLastUpdated) : that.dateLastUpdated != null)
            return false;
        if (!id.equals(that.id)) return false;
        if (!version.equals(that.version)) return false;

        return true;
    }

}
