package com.codexplo.smoronika.domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 9:39 AM
 */
@Entity
public class User extends BaseEntity {
    @Column(nullable = false, unique = true)
    @NotEmpty
    @Length(min = 6, max = 32)
    private String username;
    @Column(nullable = false)
    @NotEmpty
    @Length(min = 8)
    @Size(max = 0x100)
    private String password;
    @Transient
    private String passwordConfirmed;
    @NotEmpty
    private java.lang.String firstName;
    @NotEmpty
    private java.lang.String lastName;
    @NotEmpty
    @Email
    private java.lang.String email;

    public String getPasswordConfirmed() {
        return passwordConfirmed;
    }

    public void setPasswordConfirmed(String passwordConfirmed) {
        this.passwordConfirmed = passwordConfirmed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
