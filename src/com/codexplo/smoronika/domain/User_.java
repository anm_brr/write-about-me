package com.codexplo.smoronika.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 12/18/12
 * Time: 10:37 AM
 */
@StaticMetamodel(User.class)
public class User_ {
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, String> email;
}
