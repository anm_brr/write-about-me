package com.codexplo.smoronika.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 6:23 AM
 */

@Entity
//@Table(uniqueConstraints = {
//        @UniqueConstraint(columnNames = {"userId", "providerId", "providerUserId"}),
//        @UniqueConstraint(columnNames = {"userId", "providerId", "rank"})
//})
public class SocialUser extends BaseEntity {

    @Column(unique = true)
    private String userId;
    @Column(nullable = false, unique = true)
    private String providerId;
    @Column(unique = true)
    private String providerUserId;
    @Column(nullable = false, unique = true)
    private Long rank;
    private String displayName;
    private String profileUrl;
    private String imageUrl;
    private String accessToken;
    private String secret;
    private String refreshToken;
    private Long expireTime;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    public long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SocialUser)) return false;
        if (!super.equals(o)) return false;

        SocialUser that = (SocialUser) o;

        if (rank != that.rank) return false;
        if (accessToken != null ? !accessToken.equals(that.accessToken) : that.accessToken != null) return false;
        if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
        if (expireTime != null ? !expireTime.equals(that.expireTime) : that.expireTime != null) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null) return false;
        if (profileUrl != null ? !profileUrl.equals(that.profileUrl) : that.profileUrl != null) return false;
        if (providerId != null ? !providerId.equals(that.providerId) : that.providerId != null) return false;
        if (providerUserId != null ? !providerUserId.equals(that.providerUserId) : that.providerUserId != null)
            return false;
        if (refreshToken != null ? !refreshToken.equals(that.refreshToken) : that.refreshToken != null) return false;
        if (secret != null ? !secret.equals(that.secret) : that.secret != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;

        return true;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SocialUser");
        sb.append("{userId='").append(userId).append('\'');
        sb.append(", providerId='").append(providerId).append('\'');
        sb.append(", providerUserId='").append(providerUserId).append('\'');
        sb.append(", rank=").append(rank);
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append(", profileUrl='").append(profileUrl).append('\'');
        sb.append(", imageUrl='").append(imageUrl).append('\'');
        sb.append(", accessToken='").append(accessToken).append('\'');
        sb.append(", secret='").append(secret).append('\'');
        sb.append(", refreshToken='").append(refreshToken).append('\'');
        sb.append(", expireTime=").append(expireTime);
        sb.append('}');
        return sb.toString();
    }
}
