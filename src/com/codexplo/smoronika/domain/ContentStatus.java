package com.codexplo.smoronika.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Bazlur Rahman Rokon
 * Date: 11/29/12
 * Time: 8:25 AM
 */

public enum ContentStatus {
    PUBLISHED, UNAPPROVED, SPAM, TRASH
}
